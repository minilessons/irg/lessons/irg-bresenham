function RasterDisplay(canvasID, width, height) {
  this.width = width;
  this.height = height;
  this.canvas = document.getElementById(canvasID);
  this.context = this.canvas.getContext("2d");
  var regions = [];
  var ctx = this.context;
  this.mathematical = false;
  this.showCoordinates = true;
  this.backgroundColor = "#FFFFFF";
  this.foregroundColor = "#0000FF";
  this.gridColor = "#000000";
  this.showIdealLine = false;
  this.idealLineColor = "#FF0000";
  this.idealLineData = null;

  this.pixels = [];
  for(let r = 0; r < height; r++) {
    let row = [];
    for(let c = 0; c < width; c++) {
      row.push(0);
    }
    this.pixels.push(row);
  }

  this.clear = function() {
    for(let r = 0; r < height; r++) {
      for(let c = 0; c < width; c++) {
        this.pixels[r][c] = 0;
      }
    }
  }

  this.paint = function() {
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.context.fillStyle = "#ffffee";
    this.context.strokeStyle = "#aaaa00";

    regions.length = 0;

    let marginHorizontal = 20;
    let marginVertical = 20;

    let dispX = marginHorizontal;
    let dispW = this.canvas.width - marginHorizontal;
    let dispY = this.mathematical ? 0 : marginVertical;
    let dispH = this.canvas.height - marginVertical;

    let pxwidth = dispW / this.width;
    let pxheight = dispH / this.height;
    for(let y = 0; y < this.height; y++) {
      for(let x = 0; x < this.width; x++) {
        let xa = x;
        let ya = this.mathematical ? this.height-1-y : y;
        let xs = pxwidth * xa;
        let xe = pxwidth * (xa+1);
        let ys = pxheight * ya;
        let ye = pxheight * (ya+1);
        this.context.fillStyle = this.pixels[y][x]==0 ? this.backgroundColor : this.foregroundColor;
        this.context.fillRect(Math.round(xs)+dispX,Math.round(ys)+dispY,Math.round(xe-xs),Math.round(ye-ys));
        regions.push({x1: xs+dispX, y1: ys+dispY, x2: xe+dispX, y2: ye+dispY, x: x, y: y});
      }
    }

    this.context.strokeStyle = this.gridColor;
    for(let y = 0; y <= this.height; y++) {
      let yline = y * (dispH-1)/this.height;
      this.context.beginPath();
      this.context.moveTo(dispX+0,dispY+yline);
      this.context.lineTo(dispX+dispW, dispY+yline);
      this.context.stroke();
    }
    for(let x = 0; x <= this.width; x++) {
      let xline = x * (dispW-1)/this.width;
      this.context.beginPath();
      this.context.moveTo(dispX+xline,dispY+0);
      this.context.lineTo(dispX+xline,dispY+dispH);
      this.context.stroke();
    }

    if(this.showCoordinates) {
      this.context.fillStyle = "#000000";
      this.context.font = '10px sans';
      this.context.textAlign = 'right';      
      this.context.textBaseline='middle';
      
      for(let y = 0; y < this.height; y++) {
        let yline = (this.mathematical ? this.height-1-y : y) * dispH/this.height + dispH/this.height/2.0;
        this.context.fillText(y, marginHorizontal-3, this.mathematical ? yline : yline + marginVertical);
      }

      this.context.textAlign = 'center';      
      this.context.textBaseline=this.mathematical ? 'top' : 'bottom';
      for(let x = 0; x < this.width; x++) {
        let xline = x * dispW/this.width + dispW/this.width/2.0;
        this.context.fillText(x, marginHorizontal+xline, this.mathematical ? dispH+3 : marginVertical-3);
      }

      if(this.showIdealLine && this.idealLineData!=null) {
        this.context.strokeStyle = this.idealLineColor;
        let xl0 = this.idealLineData.x0 * dispW/this.width + dispW/this.width/2.0;
        let yl0 = (this.mathematical ? this.height-1-this.idealLineData.y0 : this.idealLineData.y0) * dispH/this.height + dispH/this.height/2.0;
        let xl1 = this.idealLineData.x1 * dispW/this.width + dispW/this.width/2.0;
        let yl1 = (this.mathematical ? this.height-1-this.idealLineData.y1 : this.idealLineData.y1) * dispH/this.height + dispH/this.height/2.0;
        this.context.beginPath();
        this.context.moveTo(dispX+xl0,dispY+yl0);
        this.context.lineTo(dispX+xl1,dispY+yl1);
        this.context.stroke();
      }
    }
    // regions.push({x1: sx-tw/2, y1: sy, x2: sx+tw/2, y2: sy+y, n: elem.n});
  }

  var me = this;

  this.canvas.addEventListener('mousedown', function(evt) {
    var rect = me.canvas.getBoundingClientRect();
    var pos = {x: evt.clientX - rect.left, y: evt.clientY - rect.top};
    var selected = null;
    for(var i = 0; i < regions.length; i++) {
      var r = regions[i];
      if(pos.x < r.x1 || pos.x > r.x2 || pos.y < r.y1 || pos.y > r.y2) continue;
      selected = r;
      break;
    }
    if(selected!=null) {
      me.paint();
      //alert("Piksel "+selected.x+", "+selected.y);
    }
  });

  //this.paint();
}

function BresenhamAlgorithm1(lineData, gridDisplay) {
  this.x = lineData.x0;
  this.a = (lineData.y1-lineData.y0)/(lineData.x1-lineData.x0);
  this.b = -this.a*lineData.x0 + lineData.y0;

  this.reset = function() {
    this.x = lineData.x0;
    this.a = (lineData.y1-lineData.y0)/(lineData.x1-lineData.x0);
    this.b = -this.a*lineData.x0 + lineData.y0;
    gridDisplay.clear();
    gridDisplay.paint();
  }
  this.hasNext = function() {
    return this.x <= lineData.x1;
  }
  this.next = function() {
    if(!this.hasNext()) return;
    let y = Math.round(this.a*this.x + this.b);
    if(y>=0 && y<gridDisplay.height && this.x>=0 && this.x<gridDisplay.width) {
      gridDisplay.pixels[y][this.x] = 1;
    }
    this.x++;
    gridDisplay.paint();
  }
}

function BresenhamAlgorithm2(lineData, gridDisplay) {
  this.x = lineData.x0;
  this.y = lineData.y0;
  this.a = (lineData.y1-lineData.y0)/(lineData.x1-lineData.x0);

  this.reset = function() {
    this.x = lineData.x0;
    this.y = lineData.y0;
    this.a = (lineData.y1-lineData.y0)/(lineData.x1-lineData.x0);
    gridDisplay.clear();
    gridDisplay.paint();
  }
  this.hasNext = function() {
    return this.x <= lineData.x1;
  }
  this.next = function() {
    if(!this.hasNext()) return;
    let y = Math.round(this.y);
    if(y>=0 && y<gridDisplay.height && this.x>=0 && this.x<gridDisplay.width) {
      gridDisplay.pixels[y][this.x] = 1;
    }
    this.x++;
    this.y+=this.a;
    gridDisplay.paint();
  }
}

function BresenhamAlgorithm3(lineData, gridDisplay) {
  this.x = lineData.x0;
  this.a = (lineData.y1-lineData.y0)/(lineData.x1-lineData.x0);
  this.yc = lineData.y0;
  this.yf = -0.5;

  this.reset = function() {
    this.x = lineData.x0;
    this.a = (lineData.y1-lineData.y0)/(lineData.x1-lineData.x0);
    this.yc = lineData.y0;
    this.yf = -0.5;
    gridDisplay.clear();
    gridDisplay.paint();
  }
  this.hasNext = function() {
    return this.x <= lineData.x1;
  }
  this.next = function() {
    if(!this.hasNext()) return;
    if(this.yc>=0 && this.yc<gridDisplay.height && this.x>=0 && this.x<gridDisplay.width) {
      gridDisplay.pixels[this.yc][this.x] = 1;
    }
    this.x++;
    this.yf+=this.a;
    if(this.yf > 0) {
      this.yf-=1;
      this.yc++;
    }
    gridDisplay.paint();
  }
}

function BresenhamAlgorithm4(lineData, gridDisplay) {
  this.x = lineData.x0;
  this.a = Math.abs(lineData.y1-lineData.y0)/(lineData.x1-lineData.x0);
  this.yc = lineData.y0;
  this.yf = -0.5;
  this.dy = lineData.y1-lineData.y0;
  this.korekcija = this.dy < 0 ? -1 : 1;

  this.reset = function() {
    this.x = lineData.x0;
    this.a = Math.abs(lineData.y1-lineData.y0)/(lineData.x1-lineData.x0);
    this.yc = lineData.y0;
    this.yf = -0.5;
    this.dy = lineData.y1-lineData.y0;
    this.korekcija = this.dy < 0 ? -1 : 1;
    gridDisplay.clear();
    gridDisplay.paint();
  }
  this.hasNext = function() {
    return this.x <= lineData.x1;
  }
  this.next = function() {
    if(!this.hasNext()) return;
    if(this.yc>=0 && this.yc<gridDisplay.height && this.x>=0 && this.x<gridDisplay.width) {
      gridDisplay.pixels[this.yc][this.x] = 1;
    }
    this.x++;
    this.yf+=this.a;
    if(this.yf > 0) {
      this.yf-=1;
      this.yc += this.korekcija;
    }
    gridDisplay.paint();
  }
}

function BresenhamAlgorithm5(lineData, gridDisplay) {
  this.y = lineData.y0;
  this.a = Math.abs(lineData.x1-lineData.x0)/(lineData.y1-lineData.y0);
  this.xc = lineData.x0;
  this.xf = -0.5;
  this.dx = lineData.x1-lineData.x0;
  this.korekcija = this.dx < 0 ? -1 : 1;

  this.reset = function() {
    this.y = lineData.y0;
    this.a = Math.abs(lineData.x1-lineData.x0)/(lineData.y1-lineData.y0);
    this.xc = lineData.x0;
    this.xf = -0.5;
    this.dx = lineData.x1-lineData.x0;
    this.korekcija = this.dx < 0 ? -1 : 1;
    gridDisplay.clear();
    gridDisplay.paint();
  }
  this.hasNext = function() {
    return this.y <= lineData.y1;
  }
  this.next = function() {
    if(!this.hasNext()) return;
    if(this.y>=0 && this.y<gridDisplay.height && this.xc>=0 && this.xc<gridDisplay.width) {
      gridDisplay.pixels[this.y][this.xc] = 1;
    }
    this.y++;
    this.xf+=this.a;
    if(this.xf > 0) {
      this.xf-=1;
      this.xc += this.korekcija;
    }
    gridDisplay.paint();
  }
}

function BresenhamAlgorithm6(lineData, gridDisplay) {

  this.reset = function() {
    var dx = lineData.x1-lineData.x0;
    var dy = lineData.y1-lineData.y0;
    var lineData2 = {
       x0: lineData.x1, y0: lineData.y1,
       x1: lineData.x0, y1: lineData.y0};
    if(Math.abs(dx) > Math.abs(dy)) {
      if(dx>=0) {
        this.alg = new BresenhamAlgorithm4(lineData, gridDisplay);
        //alert("grana1");
      } else {
        this.alg = new BresenhamAlgorithm4(lineData2, gridDisplay);
        //alert("grana2");
      }
    } else {
      if(dy>=0) {
        this.alg = new BresenhamAlgorithm5(lineData, gridDisplay);
        //alert("grana3");
      } else {
        this.alg = new BresenhamAlgorithm5(lineData2, gridDisplay);
        //alert("grana4");
      }
    }
/*
    if(lineData.x0 <= lineData.x1) {
      if(lineData.x1-lineData.x0 > Math.abs(lineData.y1-lineData.y0)) {
        this.alg = new BresenhamAlgorithm4(lineData, gridDisplay);
      } else {
        this.alg = new BresenhamAlgorithm5(lineData, gridDisplay);
      }
    } else {
      var lineData2 = {
         x0: lineData.x1, y0: lineData.y1,
         x1: lineData.x0, y1: lineData.y0};
      if(lineData.x0-lineData.x1 > Math.abs(lineData.y0-lineData.y1)) {
        this.alg = new BresenhamAlgorithm4(lineData2, gridDisplay);
      } else {
        this.alg = new BresenhamAlgorithm5(lineData2, gridDisplay);
      }
    }*/
    this.alg.reset();
  }

  this.reset();

  this.hasNext = function() {
    return this.alg.hasNext();
  }
  this.next = function() {
    this.alg.next();
  }
}

function parseLineData(prefixID, lineData, w, h) {
  let lx0 = parseInt(document.getElementById(prefixID+"_x0").value);
  let ly0 = parseInt(document.getElementById(prefixID+"_y0").value);
  let lx1 = parseInt(document.getElementById(prefixID+"_x1").value);
  let ly1 = parseInt(document.getElementById(prefixID+"_y1").value);
  if(lx0==NaN) {
    alert("x0 koordinata nije ispravan broj.");
    return false;
  }
  if(lx0<0 || lx0>=w) {
    alert("x0 koordinata je izvan dopuštenog raspona.");
    return false;
  }
  if(ly0==NaN) {
    alert("y0 koordinata nije ispravan broj.");
    return false;
  }
  if(ly0<0 || ly0>=h) {
    alert("y0 koordinata je izvan dopuštenog raspona.");
    return false;
  }
  if(lx1==NaN) {
    alert("x1 koordinata nije ispravan broj.");
    return false;
  }
  if(lx1<0 || lx1>=w) {
    alert("x1 koordinata je izvan dopuštenog raspona.");
    return false;
  }
  if(ly1==NaN) {
    alert("y1 koordinata nije ispravan broj.");
    return false;
  }
  if(ly1<0 || ly1>=h) {
    alert("y1 koordinata je izvan dopuštenog raspona.");
    return false;
  }
  lineData.x0 = lx0;
  lineData.y0 = ly0;
  lineData.x1 = lx1;
  lineData.y1 = ly1;
  return true;
}
