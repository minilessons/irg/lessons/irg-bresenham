import java.nio.file.*;

public class GenerirajRaster {

  public static void main(String[] args) throws java.io.IOException {
    final int dw = 20; // pixel width
    final int dh = 20; // pixel height
    final int w = 20;  // raster width
    final int h = 20;  // raster height
    final int marginLeft = 30;
    final int marginTop = 20;

    StringBuilder sb = new StringBuilder();
    sb.append("<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"")
      .append(w*dw+marginLeft+1)
      .append("\" height=\"")
      .append(h*dh+marginTop+1)
      .append("\">\n");

    sb.append("<defs>");
    sb.append(" <marker id=\"arrow\" markerWidth=\"10\" markerHeight=\"10\" refX=\"0\" refY=\"3\" orient=\"auto\" markerUnits=\"strokeWidth\">");
    sb.append("  <path d=\"M0,0 L0,6 L9,3 z\" fill=\"#000\" />");
    sb.append(" </marker>");
    sb.append("</defs>");

    for(int y = 0; y < h; y++) {
      for(int x = 0; x < w; x++) {
        sb.append("<rect x=\"").append(marginLeft+x*dw)
          .append("\" y=\"").append(marginTop+y*dh)
          .append("\" width=\"").append(dw)
          .append("\" height=\"").append(dh)
          .append("\" style=\"fill:white;stroke:pink;stroke-width:1\"/>\n");
      }
    }

    sb.append("<g font-family=\"Verdana\" font-size=\"12pt\" text-anchor=\"end\" >");
    for(int y = 0; y < h; y++) {
      sb.append("<text x=\"").append(marginLeft - 4)
        .append("\" y=\"").append(marginTop + y*dh + 3*dh/4)
        .append("\">").append(String.valueOf(y))
        .append("</text>\n");
    }
    sb.append("</g>");

    for(int x = 0; x < w; x++) {
      sb.append("<text x=\"").append(marginLeft + x*dw + dw/2)
        .append("\" y=\"").append(marginTop - 4)
        .append("\" font-family=\"Verdana\" font-size=\"12pt\" text-anchor=\"middle\">").append(String.valueOf(x))
        .append("</text>\n");
    }

    sb.append("<line x1=\"").append(marginLeft+dw/2)
      .append("\" y1=\"").append(marginTop+dh/2)
      .append("\" x2=\"").append(marginLeft+dw/2+(w-1)*dw)
      .append("\" y2=\"").append(marginTop+dh/2)
      .append("\" style=\"stroke:rgb(0,0,0);stroke-width:1\" marker-end=\"url(#arrow)\"/>\n");

    sb.append("<line x1=\"").append(marginLeft+dw/2)
      .append("\" y1=\"").append(marginTop+dh/2)
      .append("\" x2=\"").append(marginLeft+dw/2)
      .append("\" y2=\"").append(marginTop+dh/2+(h-1)*dh)
      .append("\" style=\"stroke:rgb(0,0,0);stroke-width:1\" marker-end=\"url(#arrow)\"/>\n");

    for(int y = 0; y < h; y++) {
      for(int x = 0; x < w; x++) {
        sb.append("<circle cx=\"").append(marginLeft+x*dw+dw/2)
          .append("\" cy=\"").append(marginTop+y*dh+dh/2)
          .append("\" r=\"2\" style=\"fill:red\"/>\n");
      }
    }


    sb.append("</svg>\n");

    Files.write(
      Paths.get("raster.svg"), 
      sb.toString().getBytes(java.nio.charset.StandardCharsets.UTF_8)
    );
  }

}

